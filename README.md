# CMS (Complaint Management System) Froented app

## Directory Structure

App
----src
--------assets
--------environments
--------modules
------------app
----------------components/
----------------app.module.ts
----------------components.b.ts
----------------index.ts
------------layout
----------------components
--------------------header
--------------------footer
--------------------nav
----------------layout.module.ts
----------------components.b.ts
----------------index.ts
------------login
----------------components/
----------------index.ts
----------------login.module.ts
------------shared
----------------components
--------------------a.component
--------------------b.component
--------------------c.component
----------------shared.module.ts
----------------components.b.ts
----------------index.ts

------------cms
----------------components/
----------------index.ts
----------------cms.module.ts

## Description

Module approach with lazy loading.
