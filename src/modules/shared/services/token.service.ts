import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

/**
 * To check if token is expired an redirect to login page...
 * 
 */
@Injectable()
export class TokenService {

  static instance1: TokenService;

  constructor(private router: Router) {
    return TokenService.instance1 = TokenService.instance1 || this;
  }

  // Observable string sources
  private tokenSubject = new Subject<string>();


  token$ = this.tokenSubject.asObservable();

  /**
   * Here we can display message
   * @param message string
   */
  setTokenMessage(message: string) {
    if (message == "jwt expired") {
      localStorage.clear
      this.router.navigate(['/auth/login']);
    }
    this.tokenSubject.next(message);
  }

  cleaToken() {
    this.tokenSubject.next();
  }

  getToken(): Observable<any> {
    return this.tokenSubject.asObservable();

  }

}
