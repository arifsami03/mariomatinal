import { Component, Input } from '@angular/core';

/**
 * Dashboard widget link
 *
 * How to use?
 *
 * <shared-dashboard-widget-link  [icon]="'list'" [label]="'my label'" [link]="['/link/to/']"></shared-dashboard-widget-link>
 */
@Component({
  selector: 'shared-dashboard-widget-link',
  templateUrl: './dashboard-widget-link.component.html',
  styleUrls: ['./dashboard-widget-link.component.css']
})
export class DBWLinkComponent {

  @Input() type = 'icon';
  @Input() icon: string;
  @Input() label: string;
  @Input() link;

  constructor() {}
}
