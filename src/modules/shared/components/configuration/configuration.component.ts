import { Component, ViewChild, OnInit, AfterViewInit, Injectable } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from 'modules/shared/helper';
import { LayoutService } from 'modules/layout/services';
import { ConfigurationService } from 'modules/shared/services';
import { ConfigurationModel } from 'modules//shared/models';
import { ConfigurationFormComponent } from './form/configuration-form.component';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'settings-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css'],
  animations: [PageAnimation]
})
export class ConfigurationComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public pageTitle: string;

  public key: string;

  displayedColumns = ['value', 'options'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  public componentLabels = ConfigurationModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public layoutService: LayoutService,
    public configurationService: ConfigurationService,
    public dialog: MatDialog
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.key = this.route.snapshot.data['key'];
    this.pageTitle = this.route.snapshot.data['title'];

    this.layoutService.setPageTitle({ title: this.pageTitle });
    this.getRecords();
  }

  getRecords(): void {
    this.configurationService.getAll(this.key).subscribe(
      response => {
        this.dataSource = new MatTableDataSource<ConfigurationModel>(response);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  openForm(action: string, value: string) {
    let dialogRef = this.dialog.open(ConfigurationFormComponent, {
      width: '500px',
      // height: '200px',
      data: { key: this.key, action: action, value: value }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getRecords();
      }
    });
  }
  /**
   * Function to delete item
   * @param id  Configuration id
   */
  deleteClick(id: number) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you permanently want to delete?' }
    });

    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.configurationService.delete(id).subscribe(
          response => {
            // Display of succes message in case of succesfull deletion
            if (response) {

              this.layoutService.flashMsg({ msg: this.pageTitle + ' has been deleted.', msgType: 'success' });

              // if (this.key == 'natureOfWork') {
              //   this.layoutService.flashMsg({ msg: 'Nature of Work has been deleted.', msgType: 'success' });
              // } else if (this.key == 'instrumentType') {
              //   this.layoutService.flashMsg({ msg: 'Instrument Type has been deleted.', msgType: 'success' });
              // } else if (this.key == 'natureOfFeeHeads') {
              //   this.layoutService.flashMsg({ msg: 'Nature of Fee Heads has been deleted.', msgType: 'success' });
              // } else if (this.key == 'typeOfFeeHeads') {
              //   this.layoutService.flashMsg({ msg: 'Type of Fee Heads has been deleted.', msgType: 'success' });
              // } else if (this.key == 'natureOfCourse') {
              //   this.layoutService.flashMsg({ msg: 'Nature of Course has been deleted.', msgType: 'success' });
              // } else {
              //   this.layoutService.flashMsg({ msg: this.key + ' has been deleted.', msgType: 'success' });
              // }
              this.getRecords();
            } else {
              // Error Display
              //  this.error = true;
              this.getRecords();
              this.errorMessage = response.error;
            }
          },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          }
        );
      }
    });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }
}
