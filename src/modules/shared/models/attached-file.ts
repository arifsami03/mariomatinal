import { FormControl, Validators } from '@angular/forms';

export class AttachedFileModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    originalName: 'File Name',
    name: 'Name',
    path: 'Path',
    size: 'Size',
    type: 'Type',
    title: 'Title',
    createdAt: 'Upload Date',
    description: 'Description'
  };

  id?: number;
  attachmentId: number;
  originalName: string;
  name: string;
  path: string;
  size: number;
  type: string;
  description: string;
  title: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() {}
}
