import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class ConfigurationOptionModel {
  id?: number;
  configurationfId?: number;
  key: string;
  value: string;

  static attributesLabels = {
    key: 'Key',
    value: 'Value',
  };

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      key: new FormControl(''),
      value: new FormControl(''),
    };
  }
}
