import { FormControl, Validators } from '@angular/forms';

export class AttachmentModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    parent: 'Parent',
    parantId: 'Parent Id',
    title: 'Title',
    description: 'Description'
  };

  id?: number;
  parent: string;
  parantId: number;
  title: string;
  description: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() {}
}
