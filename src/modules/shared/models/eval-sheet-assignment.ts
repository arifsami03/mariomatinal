import { FormControl, Validators } from '@angular/forms';

export class EvalSheetAssignmentModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    esCategoryId: 'Category',
    evaluationSheetId: 'Evaluation Sheet',
  };

  id?: number;
  esCategoryId: number;
  evaluationSheetId: number;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() {}

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      esCategoryId: new FormControl('', [Validators.required]),
      // evaluationSheetId: new FormControl('',   [Validators.required]),
    };
  }

}
