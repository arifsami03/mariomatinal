import { FormControl, Validators, ValidatorFn, FormArray } from '@angular/forms';
import { ConfigurationOptionModel } from './configuration-option';


export class ConfigurationModel {
  id?: number;
  key: string;
  value: string;
  parentKey?: string;
  parentKeyId?: number;
  configurationOptions: ConfigurationOptionModel[] = [new ConfigurationOptionModel()];

  static attributesLabels = {
    name: 'Name`',
    parentKey: 'Belongs To'
  };

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      key: new FormControl(''),
      value: new FormControl('', [<any>Validators.required]),
      parentKey: new FormControl(null),
      parentKeyId: new FormControl(null),
      configurationOptions: new FormArray([]),
    };
  }
}
