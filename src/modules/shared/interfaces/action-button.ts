export interface IActionButton {
    type: string,
    url: string,
    icon: string,
    toolTip: string,
    perm: string,
  }