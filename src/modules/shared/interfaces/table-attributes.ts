
export interface ITableAttributes {
    offset: number;
    limit: number;
    search?: string | number;
    sortAttribute?: string;
    sortDirection?: string;
}