import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';

import { AppComponent } from 'modules/app/components';

import { LayoutComponent } from 'modules/layout/components';

/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    data: { breadcrumb: { title: 'Home', display: true } },
    children: [
      {
        path: 'custom-search',
        loadChildren: 'modules/custom-search/custom-search.module#CustomSearchModule',
        pathMatch: 'full'
      },
    ]
  },

  { path: '**', redirectTo: '' }
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutes { }
