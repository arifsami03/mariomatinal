import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { BaseService } from 'modules/shared/services';

@Injectable()
export class CustomSearchService extends BaseService {

  constructor(protected http: Http) {
    super(http)
  }

  loadFileWithLastHundredLines() {
    return this.http.get('assets/remotesyslog8888.log').map(data => {
      let retData = data.text();
      let retArr = retData.split('\n').slice(-101);
      retArr.pop();
      return retArr;
    });
  }

  searchFromFile(includes: string[], excludes: string[]) {
    return this.http.get('assets/remotesyslog8888.log').map(data => {
      let retData = data.text();
      let retArr = retData.split('\n');
      retArr.pop();

      // filter for includes and excludes
      if (includes.length > 0) {
        retArr = retArr.filter(d =>
          includes.find(item => d.toLowerCase().indexOf(item.toLowerCase()) > -1) !== undefined &&
          excludes.find(item => d.toLowerCase().indexOf(item.toLowerCase()) > -1) == undefined);
      } else if (includes.length == 0 && excludes.length > 0) {
        retArr = retArr.filter(d =>
          excludes.find(item => d.toLowerCase().indexOf(item.toLowerCase()) > -1) == undefined);
      }

      return retArr;
    });
  }
}
