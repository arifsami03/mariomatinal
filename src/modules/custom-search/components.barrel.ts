// Import components and services etc here
import { TextMaskModule } from 'angular2-text-mask';

import {
  MatInputModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatDialogModule,
  MatButtonModule,
  MatCardModule,
  MatTableModule,
  MatPaginatorModule,
  MatOptionModule,
  MatAutocompleteModule,
  MatSelectModule,
  MatDividerModule,
  MatListModule,
  MatExpansionModule,
  MatCheckboxModule,
  MatProgressBarModule,
  MatRadioModule,
  MatSortModule
} from '@angular/material';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { CustomSearchService } from './services/index';

import { CustomSearchComponent } from './components';

export const __IMPORTS = [
  MatInputModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatDialogModule,
  MatButtonModule,
  MatCardModule,
  FormsModule,
  ReactiveFormsModule,
  MatTableModule,
  MatPaginatorModule,
  TextMaskModule,
  MatAutocompleteModule,
  MatOptionModule,
  MatSelectModule,
  MatDividerModule,
  MatListModule,
  FlexLayoutModule,
  MatExpansionModule,
  MatCheckboxModule,
  MatProgressBarModule,
  MatRadioModule,
  MatSortModule
];

export const __DECLARATIONS = [CustomSearchComponent];

export const __PROVIDERS = [CustomSearchService];

export const __ENTRY_COMPONENTS = [];
