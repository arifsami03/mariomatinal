import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { FormBuilder, FormArray, FormGroup } from '@angular/forms';

import { PageAnimation } from 'modules/shared/helper';
import { LayoutService } from 'modules/layout/services';
import { CustomSearchService } from 'modules/custom-search/services';
import { CustomSearch } from 'modules/custom-search/models';

import * as _ from 'lodash';

@Component({
  selector: 'custom-search',
  templateUrl: './custom-search.component.html',
  styleUrls: ['./custom-search.component.css'],
  animations: [PageAnimation]
})
export class CustomSearchComponent implements OnInit {

  public loaded: boolean = false;
  public pageState;

  public displayedColumns = ['log-detail'];
  public data: any;
  public filteredData: any;
  public dataSource: any;

  public searchView: boolean = false;

  public fg: FormGroup;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public layoutService: LayoutService,
    private customSearchService: CustomSearchService,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Log Details' });
    this.loadFileWithLastHundredLines();
  }

  loadFileWithLastHundredLines(): void {

    this.customSearchService.loadFileWithLastHundredLines().subscribe(
      response => {
        this.data = response;
      },
      error => {
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loadTable(this.data);
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  toggleSearch(param: boolean) {
    this.searchView = param;
    this.fg = this.fb.group(new CustomSearch().validationRules());
  }

  cancelSearch(param: boolean) {
    this.searchView = param;
    this.loadTable(this.data);
  }

  private loadTable(data){
    this.loaded = false;
    this.pageState = '';

    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.loaded = true;
    this.pageState = 'active';
  }

  addNewSearchItem(element: string) {
    let formElement = this.fg.get(element) as FormArray;

    formElement.push(this.fb.group({ item: '' }));
    console.log('form group: ', this.fg.controls);
  }

  removeSearchItem(element: string, index: number) {
    let formElement = this.fg.get(element) as FormArray;

    formElement.removeAt(index);
    console.log('form group: ', this.fg.controls);
  }

  customSearch(searchData: CustomSearch) {
    this.loaded = false;
    this.pageState = '';

    let includes = [];
    _.each(searchData.includes, element => { if (element['item'] != '') { includes.push(element['item']) } });

    let excludes = [];
    _.each(searchData.excludes, element => { if (element['item'] != '') { excludes.push(element['item']) } });

    this.customSearchService.searchFromFile(includes, excludes).subscribe(
      response => {

        this.filteredData = response;

      },
      error => {
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loadTable(this.filteredData);
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  sortData() {
    this.dataSource.sort = this.sort;
  }
}
