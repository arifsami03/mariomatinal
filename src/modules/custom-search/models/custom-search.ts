import { FormArray, FormGroup, FormControl } from '@angular/forms';

export class CustomSearch {

  public includes: string[];
  public excludes: string[];
  public date: string;


  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      includes: new FormArray([new FormGroup({ item: new FormControl('') })]),
      excludes: new FormArray([new FormGroup({ item: new FormControl('') })]),
      date: new FormControl(''),
    };
  }
}
