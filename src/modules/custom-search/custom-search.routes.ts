import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomSearchComponent } from './components';

/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: '',
    component: CustomSearchComponent,
  },
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomSearchRoutes { }
