export const SecurityMenu = [
  {
    heading: 'Security',
    menuItems: [
      { title: 'Users', url: ['/security/users'], perm: 'users.index' },
      { title: 'Groups', url: ['/security/groups'], perm: 'groups.index' },
      { title: 'Roles', url: ['/security/roles'], perm: 'roles.index' }
    ]
  }
];
