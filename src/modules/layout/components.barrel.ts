// Import components and services etc here
import { CommonModule } from '@angular/common';

import {
  MatSidenavModule,
  MatToolbarModule,
  MatMenuModule,
  MatListModule,
  MatButtonModule,
  MatTooltipModule,
  MatIconModule,
  MatCheckboxModule,
  MatProgressSpinnerModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutService } from 'modules/layout/services';
import { DataTableService } from 'modules/shared/services';

import {
  LayoutComponent,
  DefaultLayoutComponent,
  HeaderComponent,
  FooterComponent,
  LeftNavComponent,
  SubHeaderComponent,
  SettingsNavComponent,
  ClassicLayoutComponent,
  ClassicHeaderComponent,
  ClassicSubHeaderComponent,
  ClassicLeftNavComponent,
  ClassicFooterComponent,
  FlashMessageComponent,
  SideNavComponent,
} from './components';

export const __IMPORTS = [
  CommonModule,
  MatSidenavModule,
  MatToolbarModule,
  MatMenuModule,
  MatListModule,
  MatButtonModule,
  MatTooltipModule,
  FlexLayoutModule,
  MatIconModule,
  MatCheckboxModule,
  MatProgressSpinnerModule
];

export const __DECLARATIONS = [
  LayoutComponent,
  DefaultLayoutComponent,
  HeaderComponent,
  FooterComponent,
  LeftNavComponent,
  SettingsNavComponent,
  SubHeaderComponent,
  ClassicLayoutComponent,
  ClassicHeaderComponent,
  ClassicSubHeaderComponent,
  ClassicLeftNavComponent,
  ClassicFooterComponent,
  FlashMessageComponent,
  SideNavComponent,
];

export const __PROVIDERS = [LayoutService, DataTableService];

export const __ENTRY_COMPONENTS = [];
